# Vitaly Chekryzhev <13hakta@gmail.com>, 2013
# CDP tools: Config

import socket, struct

# Credentials to access device via http according to 15 level
pUsername = "admin"
pPassword = "XXXXXXXX"

cachefile = '/tmp/cache.txt'
cache_ttl = 86400

def get_default_gateway_linux():
	with open("/proc/net/route") as fh:
		for line in fh:
			fields = line.strip().split()
			if fields[1] != '00000000' or not int(fields[3], 16) & 2:
				continue
			return socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))
