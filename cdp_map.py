#!/usr/bin/python
# Vitaly Chekryzhev <13hakta@gmail.com>, 2013
# CDP tools: Generate Weathermap map

# ===========================

def write_graph(graph_data):
	nodeprefix = "Node"
	node_idx = 1
	for k in graph_data:
		nodename = nodeprefix + str(node_idx)
		print "NODE %s\n\tLABEL %s\n" % (nodename, hosts[k][0])
		node_idx = node_idx + 1

# ===========================

dev_addr = get_default_gateway_linux()

# Or put a ip of gateway
#dev_addr = "192.168.0.1"

print "Retrieve info from devices"

#write_graph(graph)

i = 0
graph_size = len(graph)
netmap = [0] * graph_size

print "Transform data", graph_size, len(hosts)

for k in graph:
	netmap[i] = [0] * graph_size
	netmap[i][i] = 1

	for sub in graph[k]:
		netmap[i][hosts[sub][1]] = 1
	i = i + 1

i = 0
for x in netmap:
	j = 0
	for y in x:
		print "[%s]" % y,
		j = j + 1
	print
	i = i + 1
