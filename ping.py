#!/usr/bin/python
# Vitaly Chekryzhev <13hakta@gmail.com>, 2013
# CDP tools: Ping IP from device

import sys, port_lib, port_cfg

if len(sys.argv) < 2:
	print "Usage: %s IP [root_device_IP]" % (sys.argv[0])
	sys.exit(0)

if len(sys.argv) == 3:
	dev_addr = sys.argv[2]
else:
	dev_addr = port_cfg.get_default_gateway_linux()

ip_addr = sys.argv[1]

p = port_lib.Ports(port_cfg.pUsername, port_cfg.pPassword, dev_addr)
p.cache_init(port_cfg.cachefile, port_cfg.cache_ttl)
p.verbose = False

sys.stderr.write("Ping IP: " + ip_addr + "\n")
print "\n".join(p.cmd(dev_addr, "ping " + ip_addr))
