#!/usr/bin/python
# Vitaly Chekryzhev <13hakta@gmail.com>, 2013
# CDP tools: Search port by MAC or IP

import sys, port_lib, port_cfg

if len(sys.argv) < 2:
	print "Usage: %s MAC/IP [flag] [root_device_IP]" % (sys.argv[0])
	print "flag: 0 - IP, 1 - MAC"
	sys.exit(0)

if len(sys.argv) > 2:
	flag = sys.argv[2]
else:
	flag = "0"

if len(sys.argv) == 4:
	dev_addr = sys.argv[3]
else:
	dev_addr = port_cfg.get_default_gateway_linux()

p = port_lib.Ports(port_cfg.pUsername, port_cfg.pPassword, dev_addr)
p.cache_init(port_cfg.cachefile, port_cfg.cache_ttl)
p.verbose = False

addr = sys.argv[1]

if flag == "0":
	sys.stderr.write("Search by IP: " + addr + "\n")
	port = p.lookup_port_ip(addr, True, True)
else:
	sys.stderr.write("Search by MAC: " + addr + "\n")
	port = p.lookup_port_mac(addr, True)

if port:
	if flag == "0":
		print addr + ";" + ";".join(port)
	else:
		print ";".join(port)
