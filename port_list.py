#!/usr/bin/python
# Vitaly Chekryzhev <13hakta@gmail.com>, 2013
# CDP tools: Port list

import sys, port_lib, port_cfg, socket

if len(sys.argv) < 2:
	print "Usage: %s [root_device_IP]" % (sys.argv[0])
	sys.exit(0)

if len(sys.argv) == 2:
	dev_addr = sys.argv[1]
else:
	dev_addr = port_cfg.get_default_gateway_linux()

p = port_lib.Ports(port_cfg.pUsername, port_cfg.pPassword, dev_addr)
p.cache_init(port_cfg.cachefile, port_cfg.cache_ttl)
p.verbose = False

print "IP;Switch IP;Switch name;MAC;Type;Port"

dev_list = p.get_devices()

for dev in dev_list:
	print ";".join(dev)
