#!/usr/bin/python
# Vitaly Chekryzhev <13hakta@gmail.com>, 2013
# CDP tools: Utility routines

import urllib2, os.path, sys, re, pickle, copy
from time import time

req_links = "/level/15/exec/show/cdp/neighbors/detail/|/include/IP/a|Device/ID/CR"
req_mac_filter = "/level/15/exec/show/mac/address-table/|/include/%s/CR"
req_mac = "/level/15/exec/show/mac/address-table/CR"
req_ip = "/level/15/exec/show/ip/interface/brief/|/include/\\\\./CR"
req_trunk = "/level/15/exec/show/interface/trunk/|/include/trunking/CR"
req_arp = "/level/15/exec/show/arp/|/include/Internet/CR"
req_arp_filter = "/level/15/exec/show/arp/|/include/%s/CR"

mask_mac = re.compile("\s*(\w+)\s+(\w{4}\.\w{4}\.\w{4})\s+(\w+)\s+([\/\w]+)")
mask_ip = re.compile("[\w\/]+\s+([\.\d]+)")
mask_arp = re.compile("Internet\s+([\.\d]+)\s+[\d\-]*\s+(\w{4}\.\w{4}\.\w{4})")

# =======================================

class Ports:
	def __init__(self, username, password, ip):
		self.stack = []
		self.graph = {}
		self.hosts = {}
		self.cache = {}
		self.cache_ttl = 0
		self.cache_file = None

		self.verbose = True
		self.login = username
		self.password = password
		self.top_addr = ip

		self.passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
		self.authhandler = urllib2.HTTPBasicAuthHandler(self.passman)
		self.opener = urllib2.build_opener(self.authhandler)

		urllib2.install_opener(self.opener)

# =======================================

	def __del__(self):
		self.cache_save()

# =======================================

	def get_filter_arp(self, addr):
		return req_arp_filter % (addr)

	def get_filter_mac(self, addr):
		return req_mac_filter % (addr)

# =======================================

	def cache_init(self, cachefile, ttl):
		self.cache_ttl = ttl
		self.cache_file = cachefile

		if cachefile != None:
			if os.path.exists(cachefile):
				try:
					fH = open(cachefile)
					self.cache = pickle.load(fH)
					self.hosts = self.cache['hosts']
					fH.close()

					return 0
				except:
					pass
		return -1

	def cache_save(self):
		if self.cache_file != None:
			self.cache['hosts'] = self.hosts
			try:
				fH = open(self.cache_file, 'w')
				pickle.dump(self.cache, fH)
				fH.close()
			except:
				pass

# =======================================

	def cmd_convert_cfg(self, command):
		command = command.replace(" ", "/")
		command = "/level/15/configure/" + command.replace(" ", "/") + "/CR"

		return command

# =======================================

	def cmd_convert(self, command):
		command = command.replace(" ", "/")
		command = "/level/15/exec/" + command.replace(" ", "/") + "/CR"

		return command

# =======================================

	# Execute command on device
	def cmd(self, addr, command):
		self.auth(addr)
		info = self.get_info(addr, self.cmd_convert(command))

		return info

# =======================================

	# Execute command on device
	def cmd_cfg(self, addr, command):
		self.auth(addr)
		info = self.get_info(addr, self.cmd_convert_cfg(command))

		return info

# =======================================

	# Execute command on devices
	def cmd_all(self, command):
		data = []
		if len(self.graph) == 0: self.get_topology()

		for addr in self.graph:
			data.append(self.get_info(addr, self.cmd_convert(command)))

		return data

# =======================================

	# Execute command on devices
	def cmd_all_cfg(self, command):
		data = []
		if len(self.graph) == 0: self.get_topology()

		for addr in self.graph:
			data.append(self.get_info(addr, self.cmd_convert_cfg(command)))

		return data

# =======================================

	def extract_mac(self, info, addr = None):
	# Get only data
		s = []

		for line in info:
			if line == "": continue
			keys = mask_mac.match(line)
			if keys != None:
				mac = keys.group(2)

				if addr == None or mac.find(addr) > -1:
					s.append([mac, keys.group(3), keys.group(4)])

		return s

# =======================================

	def extract_ipmac(self, info, addr):
	# Get only data
		mac = ""
		if type(info) is list:
			for line in info:
				keys = mask_arp.match(line)
				if keys != None:
					ip = keys.group(1)
					mac = keys.group(2)
					if addr == ip: break
		else:
			if info == "": return ""
			keys = mask_arp.match(info)
			ip = keys.group(1)
			mac = keys.group(2)

		return mac

# =======================================

	def extract_ip(self, info):
	# Get only data
		s = []

		for line in info:
			if line == "": continue
			keys = mask_ip.match(line)
			if keys != None:
				ip = keys.group(1)
				s.append(ip)

		return s

# =======================================

	def extract_trunk(self, info):
	# Get only data
		s = []

		for line in info:
			if line == "": continue
			port = line[0:line.find(' ')]
			s.append(port)

		return s

# =======================================

	def get_cache(self, addr, suffix = ""):
		if addr in self.cache:
			if suffix in self.cache[addr]:
				if self.cache_ttl > 0 and time() - self.cache[addr][suffix][1] > self.cache_ttl:
					del self.cache[addr][suffix]
					return []
				return self.cache[addr][suffix][0]

		return []

# =======================================

	def get_info(self, addr, req_url, suffix = ""):
		""" Read info from device/cache file """

		data = ""
		lines = []

		# Lookup in memory cache
		if suffix != "":
			lines = self.get_cache(addr, suffix)
			if len(lines) > 0: return lines

		# Otherwise get info from device or file cache

		try:
			fHandle = urllib2.urlopen('http://' + addr + req_url)
			data = fHandle.read()
			lines = data.split("\r\n")

			#Skip non CISCO devices
			if lines[1].find(' /level/') == -1: return lines

			# Extract device hostname
			hostname = lines[1][19:lines[1].find(' ')]
			self.hosts[addr] = hostname

			lines = lines[5:-4]
			if len(lines) > 0:
				lines[0] = lines[0][lines[0].find('>') + 1:]

			fHandle.close()
		except:
			if self.verbose:
				sys.stderr.write("Error! %s %s\n" % (addr, str(sys.exc_info()[1])))
				pass

		if suffix != "":
			if addr in self.cache:
				self.cache[addr][suffix] = [lines, time()]
			else:
				self.cache[addr] = {suffix: [lines, time()]}

		return lines

# =======================================

	def extract_info(self, addr, info):
		ip = ""
		dev = ""

		for line in info:
			keys = line.split(":", 1)
			if len(keys) < 2: continue
			key = keys[0].strip()
			val = keys[1].strip()

			if (key == "Device ID"):
				dev = val
				ip = ""

			if (key == "IP address"):
				ip = val

			if ip != "" and dev != "":
				if self.graph.has_key(addr):
					self.graph[addr].append(ip)
				else:
					self.graph[addr] = [ip]

				if not (self.graph.has_key(ip) or ip in self.stack):
					self.stack.append(ip)

				ip = ""
				dev = ""

# =======================================

	def auth(self, addr):
		self.passman.add_password(None, 'http://' + addr, self.login, self.password)

# =======================================

	def get_topology(self):
		excludes = []
		self.stack.append(self.top_addr)

		while len(self.stack) > 0:
			addr = self.stack.pop(0)

			if self.verbose:
				sys.stderr.write("Query device: " + addr + "\n")

			# Skip devices which has few IP
			if addr in excludes: continue

			self.auth(addr)

			# Get device IPs
			info = self.get_info(addr, req_ip, "ip")
			if len(info) == 0: continue
			excludes.extend(self.extract_ip(info))

			# List neighbors
			info = self.get_info(addr, req_links, "link")
			if info != "":
				 self.extract_info(addr, info)
			else:
				self.hosts[addr] = '-'
				self.graph[addr] = []

# ===========================

	def get_topology_short(self):
		# Get trunks without mutual repeatings
		if len(self.graph) == 0: self.get_topology()

		graph_short = {}
		excludes = []
		for dev in self.graph:
			trunks = []
			devlist = self.graph[dev]
			for d in devlist:
				if not (("%s;%s" % (d, dev) in excludes) or ("%s;%s" % (dev, d) in excludes)):
					trunks.append(d)
					excludes.append("%s;%s" % (d, dev))

			if len(trunks) > 0:
				graph_short[dev] = trunks

		return graph_short

# ===========================

	def get_topology_full(self):
		fast = True
		if len(self.graph) == 0: self.get_topology()
		graph_full = copy.deepcopy(self.graph)

		# Cache trunk ports
		trunk_ports = {}
		mac_list = {}
		for addr in self.graph:
			info = self.get_info(addr, req_trunk, "trunk")
			trunk_ports[addr] = self.extract_trunk(info)
			info = self.get_info(addr, req_mac, "mac")
			mac_list[addr] = self.extract_mac(info)

		# Loop through all ARP table
		info = self.get_info(self.top_addr, req_arp, "allip")
		for mac_rec in info:
			keys = mask_arp.match(mac_rec)
			if keys != None:
				ip = keys.group(1)
				mac = keys.group(2)

				s = None
				for addr in self.graph:
					for line in mac_list[addr]:
						if line[0] == mac and not line[2] in trunk_ports[addr]:
							s = [addr, self.hosts[addr], line[0], line[1], line[2]]
							break

				if s != None:
					graph_full[s[0]].append(ip + "|" + s[4])

		return graph_full

# ===========================

	def get_devices(self):
		fast = True
		if len(self.graph) == 0: self.get_topology()
		dev_list = []

		# Cache trunk ports and MAC list
		trunk_ports = {}
		mac_list = {}
		for addr in self.graph:
			info = self.get_info(addr, req_trunk, "trunk")
			trunk_ports[addr] = self.extract_trunk(info)
			info = self.get_info(addr, req_mac, "mac")
			mac_list[addr] = self.extract_mac(info)

		# Loop through all ARP table
		info = self.get_info(self.top_addr, req_arp, "allip")
		for mac_rec in info:
			keys = mask_arp.match(mac_rec)
			if keys != None:
				ip = keys.group(1)
				mac = keys.group(2)

				s = None
				for addr in self.graph:
					for line in mac_list[addr]:
						if line[0] == mac and not line[2] in trunk_ports[addr]:
							s = [ip, addr, self.hosts[addr], line[0], line[1], line[2]]
							break

				if s != None:
					dev_list.append(s)

		return dev_list

# ===========================

	def lookup_port_mac(self, addr_req, fast = False):
		if len(self.graph) == 0: self.get_topology()

		s = None
		# Loop all hosts
		trunk_ports = []
		for addr in self.graph:
			# Get trunks
			info = self.get_info(addr, req_trunk, "trunk")
			trunk_ports = self.extract_trunk(info)

			if fast:
				info = self.get_info(addr, req_mac, "mac")
			else:
				info = self.get_info(addr, self.get_filter_mac(addr_req))

			mac_list = self.extract_mac(info, addr_req)

			for line in mac_list:
				# Exclude trunk ports
				if not line[2] in trunk_ports:
					s = [addr, self.hosts[addr], line[0], line[1], line[2]]

		return s

# ===========================

	def lookup_port_ip(self, addr_req, fast = False, force = False):
		s = None
		self.auth(self.top_addr)

		# Ping before searching
		if force: self.cmd(self.top_addr, "ping " + addr_req)

		# Get ip-mac match
		if fast:
			info = self.get_info(self.top_addr, req_arp, "allip")
			mac_addr = self.extract_ipmac(info, addr_req)
		else:
			info = self.get_info(self.top_addr, self.get_filter_arp(addr_req))
			if len(info) == 0:
				if self.verbose:
					sys.stderr.write("Unknown IP " + addr_req + "\n")
					return

			mac_addr = self.extract_ipmac(info[0])

		s = self.lookup_port_mac(mac_addr, fast)

		if self.verbose:
			if addr_req in self.graph:
				sys.stderr.write("Trunk on device: " + self.graph[addr_req][0] + "\n")

		return s
