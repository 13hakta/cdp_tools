#!/usr/bin/python
# Vitaly Chekryzhev <13hakta@gmail.com>, 2013
# CDP tools: Execute command on all connected devices

import sys, port_lib, port_cfg

if len(sys.argv) < 2:
	print "Usage: %s CMD IP [root_device_IP]" % (sys.argv[0])
	sys.exit(0)

if len(sys.argv) == 3:
	dev_addr = sys.argv[2]
else:
	dev_addr = port_cfg.get_default_gateway_linux()

cmd = sys.argv[1]

p = port_lib.Ports(port_cfg.pUsername, port_cfg.pPassword, dev_addr)
p.cache_init(port_cfg.cachefile, port_cfg.cache_ttl)
p.verbose = False

sys.stderr.write("Execute cmd: " + cmd + "\n")
print p.cmd_all_cfg(cmd)
